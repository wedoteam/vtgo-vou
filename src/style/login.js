import { StyleSheet, Dimensions } from 'react-native'
import { Colors, TextColors } from './index'
let { width, height } = Dimensions.get('window');

export const loginHeader = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        borderBottomWidth:0,
        backgroundColor:'#00000000'
    },
    icons:{
        color:'#747474'
    }
})

export const loginStyles = StyleSheet.create({
    bgImage: {
        flex: 1,
        width: width
    },
    logo: {
        width: 126,
        height: 38,
    },
    landingContainer:{
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 56,
    },
    socialBtn:{
        width: 56,
        height: 56
    },
    roundedBg:{
        width: width,
        height: 82,
        justifyContent:'center',
        alignItems:'center'
    }
});

export const fpStyles = {
    titleViewWraps:{
        justifyContent:'center',
        alignItems: 'center',
    },
    lockIcon:{
        width: 85,
        height: 85
    }
}