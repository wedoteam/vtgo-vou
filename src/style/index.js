import {StyleSheet, Dimensions, Platform} from 'react-native';
import { Col } from 'native-base';
let { width, heigth } = Dimensions.get('window');

export const logo = require('../assets/logo.png');

export const Colors = {
    primary: '#539A2C',
    secondary: '#fff',
    bg: '#FFFFFF',
    gray: '#E9E9E9',
    border: '#EEEEEE'
};

export const defaults = {
    touchOpacity1: 0.75,
    touchOpacity2: 0.50,
    touchOpacity3: 0.25
}

export const dimentions = {
    vwidth: width,
    vheight: heigth
}

export const TextColors = {
    success: '#5C9E35',
    danger: '#D6312D',
    primary: '#121212',
    secondary: '#7D7D7D',
    light: '#fff',
    green: '#574312',
    placeholder: '#8F8F8F'
}

const baseContainerStyles = {
    flex: 1,
    backgroundColor: '#fff'
};

const flexCenter = {
    justifyContent: 'center',
    alignItems: 'center',
}

const baseBoxStyles = {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    height: 150,
    width: 150
};

export const styles = StyleSheet.create({
    container: {
        ...baseContainerStyles
    },
    loginContainer: {
        ...baseContainerStyles,
        paddingHorizontal: 56
    },
    colView:{
        flexDirection:'column'
    },
    rowView: {
        alignItems: 'center',
        flexDirection:'row'
    }
})

export const headerStyles = StyleSheet.create({
    container: {
        backgroundColor:Colors.primary,
        elevation: 0
    },
    logo:{
        width: 60,
        height: 18
    },
    icon:{
        color:'#fff'
    },
    iconImage:{
        width: 26,
        height: 26
    }
});

export const textStyles = StyleSheet.create({
    whiteText: {
        color: '#fff',
        fontSize: 16
    },
    center: {
        textAlign: 'center'
    },
    right:{
        textAlign: 'right'
    },
    left:{
        textAlign: 'left'
    },
    success:{
        color:Colors.primary
    },
    primary:{
        color: TextColors.primary,
        fontWeight: 'bold'
    },
    secondary:{
        color: TextColors.secondary,
    },
    bold:{
        fontWeight: 'bold'
    },
    h1:{
        fontSize: 22,
    },
    h2:{
        fontSize: 18,
    }
})

export const buttonStyles = StyleSheet.create({
    primaryButton: {
        backgroundColor:Colors.primary
    },
    txWhiteBtn: {
        borderColor: '#fff'
    },
    txWhiteBtnTxt :{
        color: '#fff'
    }
})