import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Text } from 'native-base'

import { Colors, textStyles } from '../../style/index'
import { fpStyles } from '../../style/login'

const lockIcon = require('../../assets/login/lock.png');

class FPTitleView extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={fpStyles.titleViewWraps}>
        <View style={{height: 75}} />
        <Image source={lockIcon} style={fpStyles.lockIcon} />
        <View style={{height: 24}} />
        <Text style={[textStyles.h1, textStyles.center, textStyles.bold]} >{this.props.title}</Text>
        <View style={{height: 8}} />
        <Text style={[textStyles.center]} >{this.props.subtitle}</Text>
      </View>
    );
  }
}

export default FPTitleView;
