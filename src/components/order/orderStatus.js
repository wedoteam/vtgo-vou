import React, { Component } from 'react'
import { View } from 'react-native'
import { Text } from 'react-native'
import I18n from '../../i18n/i18n'

import { orderList } from './styles'

export class OrderStatus extends Component {
  render() {

    let status = this.props.status;
    let textColor= '#D5211D';

    let orderStatus = I18n.t('order.status');
    switch(status){
        case 0: 
            textColor= '#148ADD';
            orderStatus = I18n.t('order.status.0');
            break;
        
        case 1: 
            orderStatus = I18n.t('order.status.1');
            break;

        case 2: 
            orderStatus = I18n.t('order.status.2');
            break;

        case 3: 
            orderStatus = I18n.t('order.status.3');
            break;

        case 4: 
            orderStatus = I18n.t('order.status.4');
            break;
    }

    return (
        <Text style={{color: textColor}} >{orderStatus}</Text>
    )
  }
}

export default OrderStatus