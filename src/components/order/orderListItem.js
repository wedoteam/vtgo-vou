import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Text, Grid, Col } from 'native-base'

import I18n from '../../i18n/i18n'
import OrderStatus from './orderStatus'

import { orderList } from './styles'
import { textStyles, style, styles } from '../../style/index'

const busIcon = require('../../assets/icons/bus.png')
const driverIcon = require('../../assets/icons/driver.png');
const ticketIcon = require('../../assets/icons/ticket.png');

export class OrderListItem extends Component {

  render() {
    let { quoteId, license, driver, orderId, status } = this.props.data;
    return (
      <View style={orderList.container}>
        <Grid>
            <Col>
                <Text style={[textStyles.h2, textStyles.bold]}>{quoteId}</Text>
            </Col>
            <Col>
                <Text style={textStyles.right} >
                    <OrderStatus status={status} />
                </Text>
            </Col>
        </Grid>
        <View style={{height: 14}} />
        <Grid >
            <Col style={{width: 82}}>
                <Image source={busIcon} style={orderList.vehicleIcon}  />
            </Col>
            <Col>
                <View style={styles.rowView}>
                    <Text>{I18n.t('order.licenseTitle')} : </Text>
                    <Text>{license}</Text>
                </View>
                <View style={styles.rowView}>
                    <Image resizeMode='contain' source={driverIcon} style={orderList.tinyIcon} />
                    <View style={{width: 8}} />
                    <Text>{I18n.t('order.driverTitle')} : </Text>
                    <Text style={textStyles.bold}>{driver}</Text>
                </View>
                <View style={styles.rowView}>
                    <Image resizeMode='contain' source={ticketIcon} style={orderList.tinyIcon} />
                    <View style={{width: 8}} />
                    <Text>{I18n.t('order.orderTitle')} : </Text>
                    <Text>{orderId}</Text>
                </View>
            </Col>
        </Grid>
        <View style={{height: 14}} />
      </View>
    )
  }
}

export default OrderListItem