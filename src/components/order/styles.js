import { StyleSheet, Dimensions } from 'react-native'
import { Colors, TextColors } from '../../style/index';

let { width, height } = Dimensions.get('window');

export const orderList = StyleSheet.create({
    container: {
        paddingHorizontal: 14,
        paddingVertical: 14,
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1,
    },
    vehicleIcon:{
        width: 66,
        height: 66
    },
    tinyIcon:{
        width: 16,
        height: 16
    }
})