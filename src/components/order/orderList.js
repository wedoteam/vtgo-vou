import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import { Text } from 'native-base'

import OrderListItem from '../../components/order/orderListItem'

import { orders } from '../../data'

class OrderList extends Component {

  constructor(props) {
    super(props);
    this.state = {
    
    };
  }

  render() {

    return (
      <FlatList
        data={orders}
        numColumns={1}
        renderItem={(item) => {
          return(
            <OrderListItem data={item.item} />
          )
        }}
      />
    );
  }
}

export default OrderList;
