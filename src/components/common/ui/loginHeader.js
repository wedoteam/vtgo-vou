import React, { Component } from 'react';
import { View, StatusBar, Platform, TouchableOpacity } from 'react-native';
import { Header, Text, Left, Body, Right, Button } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons'
import { loginHeader } from '../../../style/login'

import { Colors } from '../../../style/index'

export class LoginHeader extends Component {

  _headerContent(){
    let backIcon = 'md-arrow-back';
    if(Platform.OS == 'ios'){
        backIcon = 'ios-arrow-round-back';
    }

    let iconStyle = loginHeader.icons;
    let textStyle = loginHeader.title;

    return(
      <View style={{flex: 1, justifyContent:'space-between', flexDirection: 'row',}}>
        <Left>
            {this.props.showBack &&
                <Button transparent onPress={() => {this.props.navigation.goBack(null);}}>
                    <Icon size={24} style={iconStyle} name={backIcon} />
                </Button>
            }
        </Left>
        <Body></Body>
        <Right></Right>
      </View>
    )
  }

  render() {
    return (
      <Header noShadow style={loginHeader.container} androidStatusBarColor={Colors.primary}  >
        {this._headerContent()}
      </Header>
    )
  }
}

export default LoginHeader