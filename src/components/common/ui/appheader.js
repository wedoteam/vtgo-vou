import React, { Component } from 'react';
import { View, Image, Platform, TouchableOpacity } from 'react-native';
import { Header, Text, Body, Grid, Col, Badge } from 'native-base'
import { Colors, textStyles, styles, headerStyles } from '../../../style/index'
import Icon from 'react-native-vector-icons/Ionicons'

const logo = require('../../../assets/logo.png');
const iconSearch = require('../../../assets/icons/search.png');
const iconFilter = require('../../../assets/icons/filter.png');
const iconNotification = require('../../../assets/icons/notification.png');

export default class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  renderBadge(count){
    return (
        <Badge style={{position: 'absolute', top: 2, right: 2,zIndex: 10, width: 22, height: 22}}><Text style={{fontSize:12}}>{count}</Text></Badge>
    )
  }

  primaryHeaderContent() {

    let menuIcon = 'md-menu';
    let backIcon = 'md-arrow-back';
    let cartIcon = 'md-cart';
    if(Platform.OS == 'ios'){
        backIcon = 'ios-arrow-round-back';
        menuIcon = 'ios-menu';
        cartIcon = 'ios-cart';
    }

    return (
        <Body>
            <Grid>
                <Col style={{justifyContent:'center', width: 32}}>
                    <TouchableOpacity>
                        <Icon size={24} name={menuIcon} style={headerStyles.icon} />
                    </TouchableOpacity>
                </Col>
                <Col style={{justifyContent:'center'}}>
                    <Image source={logo} style={headerStyles.logo} />
                </Col>
                <Col style={{justifyContent:'center', width: 44, alignItems: 'center'}}>
                    <TouchableOpacity>
                        <Image resizeMode='contain' source={iconSearch} style={headerStyles.iconImage} />
                    </TouchableOpacity>
                </Col>
                <Col style={{justifyContent:'center', width: 44, alignItems: 'center'}}>
                    {this.renderBadge(2)}
                    <TouchableOpacity>
                        <Image resizeMode='contain' source={iconNotification} style={headerStyles.iconImage} />
                    </TouchableOpacity>
                </Col>
                <Col style={{justifyContent:'center', width: 44, alignItems: 'center'}}>
                  <TouchableOpacity>
                    <Image resizeMode='contain' source={iconFilter} style={headerStyles.iconImage} />
                  </TouchableOpacity>
                </Col>
            </Grid>
        </Body>
    )
  }

  render() {
    return (
      <Header androidStatusBarColor={Colors.primary} iosBarStyle='light-content' style={headerStyles.container}>
        {this.primaryHeaderContent()}
      </Header>
    );
  }
}
