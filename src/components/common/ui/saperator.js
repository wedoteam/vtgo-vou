import React, { Component } from 'react';
import { View } from 'react-native';

export default class SaperatorLine extends Component {

  render() {
    return (
      <View style={{borderBottomColor: '#ddd', borderBottomWidth: 1, flexDirection:'row'}} />
    );
  }

}
