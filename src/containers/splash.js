import React, { Component } from 'react'
import { ImageBackground, Image, StatusBar, View } from 'react-native'
import { Container, Text } from 'native-base'
import { loginStyles } from '../style/login'
import { logo, Colors, textStyles } from '../style/index'

import I18n from '../i18n/i18n'

const bg = require('../assets/login/bg.png');

export class Landing extends Component {
  
  render() {    
    return (
      <Container>
        <StatusBar backgroundColor={Colors.primary} />
        <ImageBackground source={bg} style={[loginStyles.bgImage, {alignItems:'center'}]}  >
          <View style={{height: 130}} />
          <Image source={logo} style={loginStyles.logo} />      
          <View style={{height: 16}} />
          <Text style={[textStyles.whiteText, textStyles.center]}>{I18n.t('login.splash')}</Text>

        </ImageBackground>
      </Container>
    )
  }
}

export default Landing