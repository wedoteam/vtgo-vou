import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Contentm, Text } from 'native-base'

import AppHeader from '../components/common/ui/appheader'
import OrderList from '../components/order/orderList'

class Home extends Component {
  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} />
        <OrderList />
      </Container>
    )
  }
}

export default Home