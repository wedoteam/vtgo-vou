import React, { Component } from 'react'
import { View, SafeAreaView, ImageBackground, Image, TouchableOpacity, StatusBar, KeyboardAvoidingView, Platform } from 'react-native'
import { Container, Text, Button, Input, Item, Label } from 'native-base'
import I18n from '../../i18n/i18n'
import * as Animatable from 'react-native-animatable';
import CommonLoader from '../../components/common/ui/loader'

import { bindActionCreators } from 'redux'
import * as authActions from '../../actions/auth'
import { connect } from 'react-redux'

import { loginStyles } from '../../style/login'
import { logo, textStyles, buttonStyles, Colors, defaults } from '../../style/index'

const bg = require('../../assets/login/bg.png');
const fbBtn = require('../../assets/login/facebook.png');
const googleBtn = require('../../assets/login/google.png');
const roundBg = require('../../assets/login/rounded.png');

export class Landing extends Component {

  constructor(){
    super();
    this.state = {
      username: '',
      password: ''
    }
    this.loginUser = this.loginUser.bind(this);
  }

  loginUser(){
    let data = this.state;
    this.props.authaction.loginUser(data);
  }

  componentDidMount(){
    console.log(this.props);
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor={Colors.primary} />
        <CommonLoader loading={this.props.auth.isLoading} />
        <ImageBackground source={bg} style={loginStyles.bgImage}  >
            <SafeAreaView />
            <KeyboardAvoidingView
            behavior= {(Platform.OS === 'ios')? "padding" : null}
            style={{flex: 1}}>
            <View style={loginStyles.landingContainer} >
              <View style={{flex: 1.5, justifyContent:'flex-end'}}>
                <Image source={logo} style={loginStyles.logo} />
                <View style={{height: 24}} />
              </View>

              <Animatable.View animation="fadeInDown" style={{flex: 1, justifyContent:'center', width: '100%'}}>
                <Item floatingLabel style={{borderBottomColor:'#fff', paddingBottom: 4}}>
                  <Input autoCapitalize='none' style={[textStyles.whiteText, textStyles.center]} placeholderTextColor='#fff' placeholder={I18n.t('login.enterUsername')} onChangeText={(text) => {
                    this.setState({username: text});
                  }} />
                </Item>
                <Item floatingLabel style={{borderBottomWidth: 0, marginTop: -8}} >
                  <Input autoCapitalize='none' secureTextEntry={true} style={[textStyles.whiteText, textStyles.center, {borderBottomWidth: 0}]} placeholderTextColor='#fff' bordered={false} placeholder={I18n.t('login.enterPassword')} onChangeText={(text) => {
                    this.setState({password: text});
                  }} />
                </Item>
              </Animatable.View>

              <Animatable.View animation="fadeInDown" delay={100} style={{flex: 1, justifyContent:'center', width: '100%'}}>
                <Button transparent rounded bordered block style={buttonStyles.txWhiteBtn} onPress={() => this.loginUser()}  >
                  <Text style={buttonStyles.txWhiteBtnTxt}>{I18n.t('login.loginBtn')}</Text>
                </Button>
                <View style={{height: 16}} />
                <TouchableOpacity activeOpacity={defaults.touchOpacity1} onPress={() => {
                  this.props.navigation.navigate('FPStep1');
                }} >
                  <Text style={[textStyles.whiteText, textStyles.center]}>{I18n.t('login.forgotPassword')}</Text>
                </TouchableOpacity>
              </Animatable.View>

              <View style={{flex: 1, justifyContent:'center', width: '100%', alignItems: 'center',}}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity activeOpacity={defaults.touchOpacity1}>
                    <Animatable.Image animation="fadeIn" delay={500} source={googleBtn} style={loginStyles.socialBtn} />
                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={defaults.touchOpacity1}>
                    <Animatable.Image animation="fadeIn" delay={500} source={fbBtn} style={loginStyles.socialBtn} />
                  </TouchableOpacity>
                </View>
              </View>

              <Animatable.View animation="slideInUp" delay={260} style={{flex: 1, justifyContent:'flex-end'}}>
                <ImageBackground source={roundBg} style={loginStyles.roundedBg}>
                  <TouchableOpacity activeOpacity={defaults.touchOpacity2}>
                    <Text>{I18n.t('login.noAccount')}
                      <Text style={[textStyles.success, textStyles.center, textStyles.bold]}> {I18n.t('login.register')}</Text>
                    </Text>
                  </TouchableOpacity>
                </ImageBackground>
              </Animatable.View>
            </View>
            </KeyboardAvoidingView>
        </ImageBackground>
      </Container>
    )
  }
}


export default connect(
  state => ({
      auth: state.auth
  }), 
  dispatch => ({
      authaction: bindActionCreators(authActions, dispatch)
  })
)(Landing);