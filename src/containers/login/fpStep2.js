import React, { Component } from 'react'
import { View, KeyboardAvoidingView } from 'react-native'
import { Container, Form, Item, Input, Label, Text, Button, Col } from 'native-base'
import { bindActionCreators } from 'redux'
import * as authActions from '../../actions/auth';
import { connect } from 'react-redux'

import * as Animatable from 'react-native-animatable';

import LoginHeader from '../../components/common/ui/loginHeader'
import FPTitleView from '../../components/login/fpTitleView'

import I18n from '../../i18n/i18n'
import { loginStyles } from '../../style/login'
import { styles, buttonStyles, Colors } from '../../style/index'

export class FPStep2 extends Component {

    constructor(){
        super();
        this.state = {

        }
    }

    componentDidMount(){

    }

    render() {
      return (
        <Container>
            <LoginHeader navigation={this.props.navigation} showBack={true} />
            <KeyboardAvoidingView behavior='padding' style={styles.loginContainer} >
                <FPTitleView title={I18n.t('fp.title')} subtitle={I18n.t('fp.subtitle.step2')} />

                <View style={{height: 16}} />

                <Form>
                    <Item floatingLabel >
                        <Label style={{top: 0}} >{I18n.t('fp.input.step2')}</Label>
                        <Input />
                    </Item>
                </Form>

                <View style={{height: 32}} />

                <Button style={buttonStyles.primaryButton} rounded block onPress={() => {
                    this.props.navigation.navigate('FPStep3');
                }} >
                    <Text>{I18n.t('fp.button.step2')}</Text>
                </Button>

            </KeyboardAvoidingView>
        </Container>
      )
    }
}

export default connect(
    state => ({
        auth: state.auth
    }), 
    dispatch => ({
        authaction: bindActionCreators(authActions, dispatch)
    })
)(FPStep2);