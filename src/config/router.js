import { View, Text } from 'react-native'
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation'

import Landing from '../containers/login/landing'
import Home from '../containers/home'

import FPStep1 from '../containers/login/fpStep1'
import FPStep2 from '../containers/login/fpStep2'
import FPStep3 from '../containers/login/fpStep3'

const LoginStack = createStackNavigator({
    Landing: {
        screen: Landing, 
        navigationOptions: ({ navigation }) => ({ header: null }),
    },
    FPStep1: {
        screen: FPStep1, 
        navigationOptions: ({ navigation }) => ({ header: null }),
    },
    FPStep2: {
        screen: FPStep2, 
        navigationOptions: ({ navigation }) => ({ header: null }),
    },
    FPStep3: {
        screen: FPStep3, 
        navigationOptions: ({ navigation }) => ({ header: null }),
    }
})

const MainStack = createDrawerNavigator({
    Home: {
        screen: Home, 
        navigationOptions: ({ navigation }) => ({ header: null }),
    }
},{
    drawerType : "slide",
    shadowOpacity: 1,
    drawerBackgroundColor:'#424448', 
});

export const MainContainer = createAppContainer(MainStack);
export const LoginContainer = createAppContainer(LoginStack);