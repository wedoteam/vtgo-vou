export default {  
    login: {
        splash: 'Car call application in Vietnam',
        enterUsername: 'Enter the vehicle owner account',
        enterPassword: 'Enter password',
        loginBtn: 'LOG IN',
        forgotPassword: 'Forgot password?',
        noAccount: 'No account?',
        register: 'Register Now'
    },
    fp: {
        title: 'Forgot password?',
        subtitle: {
            step1: 'Enter your email address or phone number to confirm your account',
            step2: 'Please enter the code that has been sent to your phone number',
            step3: 'Please enter a password',
        },
        input: {
            step1: 'Enter phone number or Email',
            step2: 'Enter Code',
            step31: 'Enter the new password',
            step32: 'Confirm your Password'
        },
        button: {
            step1: 'Confirm',
            step2: 'Confirm',
            step3: 'Change Password'
        }
    },
    order:{
        licenseTitle: 'License plate',
        driverTitle: 'Assigned Driver',
        orderTitle: 'Order ID',
        status: {
            0: 'Loading goods',
            1: 'Order for confirmation',
            2: 'Request, not to quote',
            3: 'High quote, need to review',
            4: 'Waiting for shipping'
        }
    }
};