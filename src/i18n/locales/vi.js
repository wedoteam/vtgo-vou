export default {  
    login: {
        splash: 'Ứng dụng gọi xe tại việt',
        enterUsername: 'Nhập tài khoản chủ xe',
        enterPassword: 'Nhập Mật khẩu',
        loginBtn: 'ĐĂNG NHẬP',
        forgotPassword: 'Quên mật khẩu?',
        noAccount: 'Chưa có tài khoản?',
        register: 'Đăng ký'
    },
    fp: {
        title: 'Quên mật khẩu?',
        subtitle: {
            step1: 'Nhập địa chỉ email hoặc số điện thoại của bạn để xác nhận tài khoản',
            step2: 'Vui lòng nhập mã Code đã được gửi tới số điện thoại của bạn',
            step3: 'Vui lòng nhập mật khai mối'
        },
        input: {
            step1: 'Nhập số điện thoại hoặc Email',
            step2: 'Nhập mã Code',
            step31: 'Nhập mật khẩu mới',
            step32: 'Nhập lại mật khẩu'
        }, 
        button: {
            step1: 'Xác nhận',
            step2: 'Xác nhận',
            step3: 'Đổi mật khẩu'
        }
    },
    order:{
        licenseTitle: 'Biển số xe',
        driverTitle: 'Tài xế đang kết nối',
        orderTitle: 'Đơn hàng',
        status: {
            0: 'Đang chở hàng',
            1: 'Đơn hàng cho xác nhận',
            2: 'Có yêu cầu chưa báo giá',
            3: 'Báo giá cao, cần xem lại bg',
            4: 'Đang chờ vận chuyển'
        }
    }
};