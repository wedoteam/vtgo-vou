import {
    LOGIN_USER, LOGIN_USER_FAILED, LOGIN_USER_SUCCESS,
    RESTORE_SESSION, RESTORE_SESSION_FAILED, RESTORE_SESSION_SUCCESS,
    LOGOUT_USER
} from '../actions/auth';

const initialState = {
    isAuth: false,
    isLoading: false,
    restoringSession: false,
    error: '',
    user: [],
    token: ''
}

export default function auth(state= initialState, action={}){
    switch(action.type){
        
        case LOGIN_USER: 
            return {
                ...state, 
                isLoading: true
            }
            break;
        
        case LOGIN_USER_SUCCESS: 
            return {
                ...state, 
                isLoading: false,
                user: action.data,
                token: action.token,
                isAuth: true
            }
            break;
        
        case LOGIN_USER_FAILED: 
            return {
                ...state, 
                error: action.error,
                isLoading: false,
                isAuth: false
            }
            break;

        case LOGOUT_USER:
            return {
                ...state,
                isAuth: false,
                user:[],
                token: ''
            }
            break;

        case RESTORE_SESSION:
            return{
                ...state,
                restoringSession: true
            }
            break;
        
        case RESTORE_SESSION_SUCCESS:
            return{
                ...state,
                restoringSession: false,
                user: action.data,
                token: action.token,
                isAuth: true
            }
            break;
        
        case RESTORE_SESSION_FAILED:
            return{
                ...state,
                restoringSession: false,
                isAuth: false
            }
            break;

        default:
            return state;

    }
}