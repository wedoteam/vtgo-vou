import { AsyncStorage } from 'react-native';
import { DATA_SESSION } from '../config/global'
import { Toast } from 'native-base';

export const LOGIN_USER="LOGIN_USER";
export const LOGIN_USER_SUCCESS="LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILED="LOGIN_USER_FAILED";
export const RESTORE_SESSION="RESTORE_SESSION";
export const RESTORE_SESSION_SUCCESS="RESTORE_SESSION_SUCCESS";
export const RESTORE_SESSION_FAILED="RESTORE_SESSION_FAILED";
export const LOGOUT_USER="LOGOUT_USER";

export function logoutUser(){
    return dispatch => {
        AsyncStorage.removeItem(DATA_SESSION).then(respnse => {
            dispatch(logoutUserSuccess())
        }).catch(error => {

        })
    }
}
function logoutUserSuccess(){
    return {
        type: LOGOUT_USER
    }
}

export function loginUser(data){
    return dispatch => {
        dispatch(loginRequest());
        setTimeout(function(){
            if(data.username == 'demo' && data.password == 'demo'){
                let data = {
                    name: 'JohnDoe',
                    email: 'johndoe@example.com',
                }
                let token = '123456789ABCD';
                
                let userdata = {
                    user : data,
                    token: token
                }
                AsyncStorage.setItem(DATA_SESSION, JSON.stringify(userdata))
                .then(() => dispatch(loginSuccess(token, data)))
                .catch(() => dispatch(loginFailed('Error occured. Please try again.')))
                
            } else {
                Toast.show({
                  text: 'Invalid Credentials. Please try again.',
                  buttonText: 'Okay'
                })
                dispatch(loginFailed('Invalid Credentials. Please try again.'));
            }
        }, 1500);
    }
}

function loginSuccess(token, data){
    return {
        type: LOGIN_USER_SUCCESS,
        data,
        token
    }
}
function loginFailed(error){
    return {
        type: LOGIN_USER_FAILED,
        error
    }
}
function loginRequest(error){
    return {
        type: LOGIN_USER
    }
}

export function restoreSession(){
    return dispatch => {
        dispatch(restoringSession());
        setTimeout(function(){
            AsyncStorage.getItem(DATA_SESSION).then(response => {
                let data = JSON.parse(response);
                if(data.token){
                    dispatch(restoreSuccess(data.user, data.token));
                } else {
                    dispatch(restoreFailed('Token not found.'));
                }
            }).catch(error => {
                dispatch(restoreFailed('Session expired.'));
            })
        }, 1000);
    }
}

function restoringSession(){
    return {
        type: RESTORE_SESSION
    }
}
function restoreSuccess(data,token){
    return {
        type: RESTORE_SESSION_SUCCESS,
        data,
        token
    }
}
function restoreFailed(error){
    return {
        type: RESTORE_SESSION_FAILED,
        error
    }
}